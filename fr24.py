import json
import time
from datetime import datetime

import matplotlib.pyplot as plt
import pandas as pd
import requests
from requests.exceptions import ReadTimeout


def area(bound):
    headers = {
        'Host': 'data-live.flightradar24.com',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:55.0) Gecko/20100101 Firefox/55.0',
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Accept-Language': 'zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3',
        'Referer': 'https://www.flightradar24.com/24.61,121.52/8',
        'Origin': 'https://www.flightradar24.com',
        'DNT': '1',
        'Connection': 'keep-alive',
    }

    params = (
        ('bounds', bound),
        ('faa', '1'),
        ('mlat', '1'),
        ('flarm', '1'),
        ('adsb', '1'),
        ('gnd', '1'),
        ('air', '1'),
        ('vehicles', '1'),
        ('estimated', '1'),
        ('maxage', '14400'),
        ('gliders', '1'),
        ('stats', '1'),
    )
    try:
        r = requests.get('https://data-live.flightradar24.com/zones/fcgi/feed.js',
                         headers=headers, params=params, timeout=2)
        j = r.json()
        return j
    except ReadTimeout as e:
        pass
    except OSError as e:
        pass


def airport(mode='arrivals', p=1):
    headers = {
        'Host': 'api.flightradar24.com',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:60.0) Gecko/20100101 Firefox/60.0',
        'Accept': 'application/json',
        'Accept-Language': 'zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3',
        'Referer': f'https://www.flightradar24.com/airport/tpe/{mode}',
        'origin': 'https://www.flightradar24.com',
        'DNT': '1',
        'Connection': 'keep-alive',
    }

    params = (
        ('code', 'TPE'),
        ('plugin[schedule]', 'schedule'),
        ('plugin[details]', 'details'),
        ('plugin-setting[schedule][mode]', mode),
        ('plugin-setting[schedule][timestamp]', time.time()),
        ('page', p),
        ('limit', '100'),
        ('token', 'Skw5RZuaHBtPEBsu8jae49b5KhxNt1B2vNWUpOwJHUY'),
    )

    r = requests.get('https://api.flightradar24.com/common/v1/airport.json',
                     headers=headers, params=params)
    text = r.text.replace('null', '\"\"')
    j = json.loads(text)
    print(j)

    schedule_arrival = j['result']['response']['airport']['pluginData']['schedule'][mode]
    item = schedule_arrival['item']
    page = schedule_arrival['page']
    data = schedule_arrival['data']
    print(item, page)

    df = pd.io.json.json_normalize(data)
    # print(df['flight.identification.number.default'])

    if p > page['total']:
        return None
    else:
        return df


if __name__ == '__main__':
    mode = ['arrivals', 'departures']
    mode = ['arrivals']
    whole = pd.DataFrame()
    for m in mode:
        for i in range(1, 2):
            df = airport(mode=m, p=i)
            if df is None:
                break
            whole = pd.concat([whole, df], sort=False)

    g = whole.groupby('flight.airport.origin.position.country.name')

    count = g.size()
    print(count)
    # count.plot.bar()
    # plt.show()
