import os
import requests


def relocate(key):
    url = f'https://skyvector.com/api/search?q={key}&i=304&z=1&ck=&lat=24.1&lon=122.3&rand=53918'
    r = requests.get(url)
    try:
        j = r.json()[0]
    except IndexError as e:
        print(r.json())
        return

    name = j['name']
    lon, lat = j['geom']
    lon = float(lon)
    lat = float(lat)
    with open(fn, 'a') as f:
        m = f'{key.strip()},,2,1,{lat:.4f},{lon:.4f},0'
        print(m)
        print()
        f.write(m+'\n')


if __name__ == '__main__':
    fn = 'taipei.wpt' 
    if os.path.exists(fn):
        os.remove(fn)

    with open('wp.list') as f:
        a = sorted(set(f.readlines()))

        for l in a:
            print(l, end='')
            relocate(l)
