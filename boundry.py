import csv
import os
from os import listdir
from os.path import isfile, isdir, join

from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

FIR = Polygon([(29, 123), (29, 124), (23, 124), (21, 122), (21, 117.30),
               (23, 117.30), (23, 118), (28, 123)])


def inside(lat, lon):
    if FIR.contains(Point(float(lat), float(lon))):
        return 1
    else:
        return -1


def check(fn):
    with open(fn, newline='') as f:
        track = csv.DictReader(f)
        status = 0
        msg = ''
        print('!', fn)
        for row in track:
            lat, lon = row['Position'].split(',')

            res = inside(lat, lon)
            if res == -status:
                msg += f"{status}, {lat}, {lon}, {row['UTC']}\n"

            status = res

        if msg != '':
            print(msg)


def traverse(path):
    dirs = listdir(path)
    for f in dirs:
        p = f'{path}/{f}'
        if isdir(p):
            traverse(p)
        if isfile(p) and 'csv' in p:
            check(p)


if __name__ == '__main__':
    traverse('db')
