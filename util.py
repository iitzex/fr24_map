def waypoint(ax):
    xi = []
    yi = []
    with open('taipei.wpt') as f:
        wps = f.readlines()
        for i, w in enumerate(wps):
            info = w.split(',')
            y = float(info[4])
            yi.append(y)
            x = float(info[5])
            xi.append(x)
            # ax.annotate(info[0], (x+0.02, y), color='k', alpha=0.4, **wp_font) # label

        ax.scatter(xi, yi, color='k', alpha=0.2,
                   marker='^', s=4, facecolors='none')


def airline(ax):
    wps = {}
    with open('taipei.wpt') as f:
        for w in f.readlines():
            name, _, _, _, lon, lat, _ = w.split(',')
            wps.update({name: (lon, lat)})

    with open('line.list') as f:
        lines = f.readlines()
        for l in lines:
            p = l.split(',')
            a = wps[p[1].strip()]
            b = wps[p[2].strip()]

            ax.plot([float(a[1]), float(b[1])], [float(a[0]),
                                                 float(b[0])], 'k--', linewidth=1, alpha=0.1)
