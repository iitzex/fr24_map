import csv
import os
import time
from datetime import datetime
from math import cos, pi, radians, sin

import cartopy
import cartopy.crs as ccrs
import matplotlib.animation as animation
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

from airport import ICAO
from ts import conv
from util import airline, waypoint

wp_font = {'size': '6'}
lbl_font = {'size': '7'}
ANR = (120, 124, 24.3, 28)
AWR = (119, 121.5, 22.7, 25)
ASR = (117, 120.5, 20.7, 24)
APP = [120, 122.3, 24.3, 25.7]
area = APP

# area =
HISTORY = 13000
SPEED = 4
X = 1.875 * SPEED

tile = ccrs.PlateCarree()
fig, ax = plt.subplots(figsize=(14, 10), subplot_kw=dict(projection=tile))


def init(ts):
    global ax
    ax.clear()
    ax.coastlines('50m', alpha=0.1)
    ax.set_extent(area, crs=tile)
    waypoint(ax)
    airline(ax)
    fig.tight_layout()

    l = mpatches.Patch(color='black', label=ts)
    plt.legend(handles=[l])


def traffic(ix, st):
    if st == 'play':
        fn = 'ac.csv'
        k = 0
        ts = datetime.now()
    else:
        global X
        path = 'acdb/'
        d = sorted(os.listdir(path))
        i = int(ix*X + HISTORY)
        fn = path + d[i]
        ts = f'{conv(d[i][:-4])} {i}'
        print(i, fn, ts)

    init(ts)

    try:
        with open(fn) as f:
            rows = csv.reader(f)
            for row in rows:
                if row[6][:-2] == '':
                    continue
                cs = row[1]
                y = float(row[2])
                x = float(row[3])
                hdg = int(row[4])
                spd = int(row[5])
                lvl = int(row[6][:-2])
                src = row[7]
                dst = row[8]
                typ = row[9]
                ver = int(row[10])

                icao_src = ICAO(src)
                icao_dst = ICAO(dst)

                if icao_src == 'RCTP':
                    c = '#37acdd'
                elif icao_dst == 'RCTP':
                    c = '#f9a245'
                elif icao_src[:2] == 'RC':
                    c = '#008080'
                elif icao_dst[:2] == 'RC':
                    c = '#ff6666'
                else:
                    c = '#999999'

                txt = cs
                txt += f'\nH{hdg:03}F{lvl}'
                if ver > 0:
                    txt += '^'
                elif ver < 0:
                    txt += 'v'
                txt += f'\n{spd} {typ} {icao_dst}'

                lth = 0.07*(spd/480.0)
                # if dst[i] == 'TPE' or src[i] == 'TPE':
                if lvl > 100:
                    ax.scatter(x, y, color='k', marker='.', alpha=1)
                    ax.plot([x, x+lth*sin(radians(hdg))],
                            [y, y+lth*cos(radians(hdg))],
                            color='r', linewidth=1, alpha=1)
                    ax.annotate(txt, (x+0.02, y), color=c, **lbl_font)

    except Exception as e:
        print(e)


def play():
    anim = animation.FuncAnimation(fig, traffic, fargs=['play'], interval=1000)
    plt.show()


def replay():
    anim = animation.FuncAnimation(fig, traffic, fargs=['replay'], interval=1)

    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=5, bitrate=1800)
    anim.save_count = 200000
    # anim.save('app.mp4', writer=writer)
    plt.show()


if __name__ == '__main__':
    replay()
    # play()
