import os
import subprocess
import time
from datetime import datetime

LAST = 1534331974


def clean():
    path = 'acdb/'
    for f in sorted(os.listdir(path)):
        if int(f[:-4]) < LAST:
            os.remove(path+f)
            print(f'removing {f}')


def day_ts(key, f):
    dst = f'ac.{key}'
    if not os.path.isdir(dst):
        os.mkdir(dst)

    path = 'acdb/'
    os.rename(path+f, dst+'/'+f)
    print(f'moving {f}')


if __name__ == '__main__':
    path = 'acdb/'
    for f in os.listdir(path):
        key = time.strftime('%m%d', time.localtime(int(f[:-4])))
        day_ts(key, f)
