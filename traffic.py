import time
from collections import OrderedDict
from datetime import date, datetime
from operator import itemgetter

from json import JSONDecodeError

import fr24
from boundry import inside


def get(ts=0):
    total = 0
    TPE = {}
    try:
        bound = '29,21,117.3,124'
        j = fr24.area(bound)
        if not j:
            return

        csvlines = []
        for k, v in j.items():
            if k == 'full_count' or k == 'version' or k == 'stats' or k == 'visible' or k == 'selected-aircraft':
                continue

            lat = float(v[1])
            lon = float(v[2])
            hdg = int(v[3])
            lvl = int(v[4])
            spd = int(v[5])
            typ = v[8]
            src = v[11]
            dst = v[12]
            ver = int(v[15])
            csn = v[16]

            if lvl == 0:
                continue

            if inside(lat, lon) == -1:
                continue

            total += 1
            line = f'{ts},{csn},{lat},{lon},{hdg},{spd},{lvl},{src},{dst},{typ},{ver}\n'
            csvlines.append(line)
            print(line, end='')

        print(total)
        return csvlines

    except OSError:
        pass
    except JSONDecodeError:
        pass
    return None


def loop():
    while(True):
        csvlines = get()
        if csvlines:
            with open('ac.csv', 'w') as f:
                f.writelines(csvlines)
        time.sleep(1)


def save():
    while(True):
        ts = str(int(time.time()))
        print(ts)
        csvlines = get(ts)
        if csvlines:
            with open(f'acdb/{ts}.csv', 'w') as f:
                f.writelines(csvlines)
        time.sleep(2)


if __name__ == '__main__':
    # save()
    loop()
