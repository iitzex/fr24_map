import pandas as pd


def ICAO(iata):
    try:
        df = pd.read_csv('airport.list', index_col='iata')
        return df.loc[iata][0]
    except KeyError as e:
        return ''


if __name__ == '__main__':
    a = ICAO('TSA')
    print(a)
