import cartopy
import cartopy.crs as ccrs
import matplotlib.animation as animation
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import pandas as pd

from util import airline, waypoint

wp_font = {'fontname': 'Arial', 'size': '6'}
lbl_font = {'fontname': 'Arial', 'size': '7'}
area = [120.1, 121.9, 24.5, 26]
history = 0
X = 1.875 * 2
RCTP = (25.0512, 121.1387)


def init(label):
    fig, ax = plt.subplots(figsize=(10, 7), subplot_kw=dict(
        projection=ccrs.PlateCarree()))

    ax.coastlines('50m', alpha=0.1)
    ax.set_extent(area, crs=ccrs.PlateCarree())
    waypoint(ax)
    airline(ax)
    fig.tight_layout()
    l = mpatches.Patch(color='black', label=label)
    plt.legend(handles=[l])

    return fig, ax


def profile():
    df = pd.read_csv('all.csv')
    dst = df.groupby('dst')
    tpe = dst.get_group('TPE')
    f = tpe.groupby('csn')

    for i, v in enumerate(f.groups):
        print('-', i, v, f.get_group(v).tail(2))
        g = f.get_group(v)

        cs = g['csn'].iloc[0]
        fig, ax = init(cs)
        ax.scatter(g['lon'], g['lat'], color='black', alpha=0.4,
                   marker='.', s=5, facecolors='none')
        # ax.plot(g['lon'], g['lat'], color='black', alpha=0.2,
        #         marker='.')

        fig.savefig('pic/'+cs+'.png')

    # plt.show()

    return g


def get_group(csn):
    df = pd.read_csv('all.csv')
    r = df.groupby('csn')
    g = r.get_group(csn)

    r = df.groupby('dst')
    g = r.get_group('TPE')
    f = g.groupby('csn')

    # print(f.groups)
    for i, v in enumerate(f.groups):
        print('-', i, v, f.get_group(v).tail(2))

    return g


def update():
    init()
    csn = 'SIA878'
    g = get_group(csn)
    print(g['spd'])
    print(len(g))
    for i in range(len(g)):
        print(g.iloc[i])

    ax.scatter(g['lon'], g['lat'], color='k', alpha=0.2,
               marker='.', s=4, facecolors='none')
    for i, (x, y, s, l) in enumerate(zip(g['lon'], g['lat'], g['spd'], g['lvl'])):
        mod = 5
        if i % mod == 0:
            info = f'{s}\n{l}'
            ax.annotate(info, (x+0.1, y-0.1), color='r',
                        alpha=0.4, **wp_font)  # label
    plt.show()


if __name__ == '__main__':
    profile()
